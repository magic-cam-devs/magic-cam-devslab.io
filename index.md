---
layout: home
---

# Mới nhất

- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
- [Ứng dụng Magic Cam (APK)](/assets/2020-06-08/magic-cam.apk)
- [Trello nhóm - MagicCam · Aged face filter](https://trello.com/b/MUt5dy9H/magiccam-aged-face-filter)
- Mã nguồn: [Ứng dụng Android](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/magiccam) **,**  [Api Server](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/flask-server), [Trang Release](https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io)
- [Link Git chứa toàn bộ tài liệu, mã nguồn luận văn](https://gitlab.com/magic-cam-devs/magiccam)

---

# Lịch Sử Thay Đổi

## 18/06/2020

- [**Release note**](/assets/2020-06-18/release_note.txt)
- [**Ứng dụng Magic Cam 2.2**](/assets/2020-06-18/magic-cam.apk)
- CycleGAN Server Domain (only access through api, testing)
````
https://cyclegan-serving.herokuapp.com
````

## 08/06/2020

- [**Release note**](/assets/2020-06-08/release_note.txt)
- [**Ứng dụng Magic Cam 2.2**](/assets/2020-06-08/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
- [Trello nhóm - MagicCam · Aged face filter](https://trello.com/b/MUt5dy9H/magiccam-aged-face-filter)
- Mã nguồn: [Ứng dụng Android](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/magiccam) **,**  [Api Server](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/flask-server), [Trang Release](https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io)
- [Link Git chứa toàn bộ tài liệu, mã nguồn luận văn](https://gitlab.com/magic-cam-devs/magiccam)

## 04/06/2020

- [**Release note**](/assets/2020-06-04/release_note.txt)
- [**Ứng dụng Magic Cam 2.1.8**](/assets/2020-06-08/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
- [Trello nhóm - MagicCam · Aged face filter](https://trello.com/b/MUt5dy9H/magiccam-aged-face-filter)
- Mã nguồn: [Ứng dụng Android](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/magiccam) **,**  [Api Server](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/flask-server), [Trang Release](https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io)
- [Link Git chứa toàn bộ tài liệu, mã nguồn luận văn](https://gitlab.com/magic-cam-devs/magiccam)

## 21/05/2020

- [**Release note**](/assets/2020-05-21/release_note.txt)
- [**Tài liệu luận văn chương 2**](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Mot_so_khai_niem_chuong_2.pdf)
- [**Ứng dụng Magic Cam 2.1.7**](/assets/2020-05-21/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
- [Trello nhóm - MagicCam · Aged face filter](https://trello.com/b/MUt5dy9H/magiccam-aged-face-filter)
- Mã nguồn: [Ứng dụng Android](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/magiccam) **,**  [Api Server](https://gitlab.com/magic-cam-devs/magiccam/-/tree/master/Source/flask-server), [Trang Release](https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io)
- [Link Git chứa toàn bộ tài liệu, mã nguồn luận văn](https://gitlab.com/magic-cam-devs/magiccam)

## 10/05/2020

- [**Release note**](/assets/2020-05-10/release_note.txt)
- [**Tập tin PoC**](https://mybinder.org/v2/gl/magic-cam-devs%2Fmagiccam-demo/master?filepath=stargan002_test.ipynb)
- [**Ứng dụng Magic Cam 2.1.5**](/assets/2020-05-10/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)

## 07/05/2020

- [**Release note**](/assets/2020-05-07/release_note.txt)
- Api Server Domain (only access through api)
````
https://magiccam.herokuapp.com
````
- [**Stargan server 0.0.2**](https://hub.docker.com/layers/kimninh1610/stargan_serving/0.0.2/images/sha256-8f23da5800788c04eb92d46aa5de73e04a6b6c237828499e81686144b8ddfa8f?context=explore)
- [**Ứng dụng Magic Cam 2.1.4**](/assets/2020-05-07/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)

## 30/04/2020

- [**Release note**](/assets/2020-04-30/release_note.txt)
- [**StarGan Server 0.0.1**](https://hub.docker.com/layers/kimninh1610/stargan_serving/0.0.1/images/sha256-eb19195aad39caf9db14a7102001373527931b2e6867bd990cf60ec888ba749b?context=explore)
- [**Ứng dụng Magic Cam 2.1.3**](/assets/2020-04-09/magic-cam.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)

## 09/04/2020
- [**Ứng dụng Magic Cam 2.1.2**](/assets/2020-04-09/magic-cam.apk)
- Mô hình đề xuất: [Face-Aging-CAAE](https://colab.research.google.com/drive/1MfOj1atePCE3XhYVtgDMo03cZl9HyKbR?usp=sharing)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)

## 02/04/2020
- [**Ứng dụng Magic Cam 2.1.1**](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Source/apk/magic-cam-debug.apk)
- [Đề cương luận văn (PDF)](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
## 26/03/2020
- [**Ứng dụng Magic Cam 2.1**](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Source/apk/magic-cam-debug.apk)
- [**Đề cương luận văn (PDF)**](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/De_cuong_luan_van.pdf)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)

## 12/03/2020
- Ứng dụng Magic Cam - [Tải APK Phiên bản 2.0](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Source/apk/magic-cam-debug.apk)
- [Slide Giới Thiệu Luận Văn](https://gitlab.com/magic-cam-devs/magiccam/-/raw/master/Document/Thesis_introducing.pptx)
