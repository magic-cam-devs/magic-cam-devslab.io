# Kiến Trúc

## File Init.json
- Client mặc định truy cập và tải về file này lần đầu tiên để init app
- Client dùng file này để lấy url của file info server
- File Init mẫu:
```
{
// Trường "url" quy định địa chỉ file json miêu tả thông tin server
  "url":"https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io/-/raw/master/assets/server/info_experimental_v1.json"
}


```

## File Info
- File info miêu tả thông tin của server
- Hiện tại chỉ mô tả các filter của server, cùng cách hiển thị chúng trong app
- File Info mẫu:
```
{
  "filters": [ // Danh sách filter, cứ mỗi item chiếm thêm một row trong list main filer
    {
      "id": 1, // id của filer, không trùng nhau
      "name": "Premium", // tên hiển thị
      "iconIsResource": false, // trường icon hay image có phải là drawable resource trong client hay không, nếu không thì đó là 1 url
      // trường icon sẽ quy định filter hiển thị dạng icon
      "icon": "https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io/-/raw/master/assets/images/star.png", 
      "mode":"grid", // trang hiển thị filter con của filter này show dưới dạng grid
      "image": null, // trường image sẽ quy định filter hiển thị dạng image
      "url": null, // nếu đây là end-point-filer, thì đây là url để load filter
      "filters": [ // Danh sách các filter con của filter này, nếu filter hiện tại là group-filter
        {
        // Filter con, có cấu trúc giống filter mẹ (composite)
          "id": 2,
          "name": "Hair colors",
          "iconIsResource": true,
          "icon": "ic_face_yellow",
          "mode": "list",
          "image": null,
          "url": null,
          "filters": [
            {
              "id": 3,
              "name": "Original",
              "iconIsResource": true,
              "icon": null,
              "mode": "list",
              "image": "color00",
              "url": "https://magiccam.herokuapp.com/haircolor-gender-age?attrs=origin"
            },
                {
              "id": 4,
              "name": "Black hair",
              "iconIsResource": true,
              "icon": null,
              "mode": "list",
              "image": "color01",
              "url": "https://magiccam.herokuapp.com/haircolor-gender-age?attrs=black_hair&attrs=male"
            },
                {
              "id": 5,
              "name": "Blond hair",
              "iconIsResource": true,
              "icon": null,
              "mode": "list",
              "image": "color02",
              "url": "https://magiccam.herokuapp.com/haircolor-gender-age?attrs=blond_hair&attrs=male"
            },
                {
              "id": 7,
              "name": "Brown hair",
              "iconIsResource": true,
              "icon": null,
              "mode": "list",
              "image": "color03",
              "url": "https://magiccam.herokuapp.com/haircolor-gender-age?attrs=brown_hair&attrs=male"
            }
          ]
        }
      ]
    }
  ]
}

```

## Tải APK
```
https://gitlab.com/magic-cam-devs/magic-cam-devs.gitlab.io/-/raw/master/assets/2020-06-18/magic-cam.apk?inline=false
```
